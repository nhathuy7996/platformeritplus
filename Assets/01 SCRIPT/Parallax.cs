﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] List<Transform> GBs = new List<Transform>();
    [SerializeField] List<float> Scales = new List<float>();
  

    Vector2 _Parallax = Vector2.zero;
    Vector2 PeriousCamPos;
    // Start is called before the first frame update
    void Start()
    {
        PeriousCamPos = Camera.main.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        _Parallax = (Vector2)Camera.main.transform.position - PeriousCamPos;
        for (int i = 0; i< GBs.Count; i++)
        {
            if (GBs == null)
                continue;
            Vector2 position = (Vector2)GBs[i].position + _Parallax;

            GBs[i].position = Vector3.Lerp(GBs[i].position, position, Scales[i] * Time.deltaTime);
        }

        PeriousCamPos = Camera.main.transform.position;
    }
}
