﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBG : MonoBehaviour
{
    [SerializeField] Vector2 SpawPos;
    [SerializeField] Transform SingleBG2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
     
        SingleBG2.position = this.transform.position + (Vector3)SpawPos * PlayerController.Instant.transform.localScale.x;
    }
}
