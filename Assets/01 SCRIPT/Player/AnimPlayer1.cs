﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimPlayer1 : AnimControl_base
{
    Animator Anim;

    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i< (int)AnimControl_base.STATE.len; i++)
        {
            AnimControl_base.STATE stateEnum = (AnimControl_base.STATE)i;
            if (playerState == stateEnum)
            {
                Anim.SetBool(stateEnum.ToString(), true);
            }
            else
            {
                Anim.SetBool(stateEnum.ToString(), false);
            }
        }

        Anim.SetFloat("SHOOT_STATE", JoyStick.Instant.GetJoyVectorRaw().x != 0? 1: 0);

        if (playerState == STATE.CLIMB)
        {
            if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0 ||
                JoyStick.Instant.GetJoyVectorRaw().y != 0 || JoyStick.Instant.GetJoyVectorRaw().x != 0)
            {
                Anim.speed = 1;
            }
            else
            {
                Anim.speed = 0;
            }

        }
    }

    public void JumpComplete()
    {
        Debug.Log("Jmp complete!");
    }
}
