﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Singleton<PlayerController>
{
    
    Rigidbody2D rigi;
    [SerializeField] float Speed, JumpForce;
    Vector2 movement = Vector2.zero;
    [SerializeField] bool IsGround = false, IsLadder = false;
    [SerializeField]
    Collider2D Colli;
    [SerializeField] public  AnimControl_base AnimController;


    bool _IsController = true;

    float GravityScaleBackup;
    // Start is called before the first frame update
    void Start()
    {
        rigi = this.GetComponent<Rigidbody2D>();
        Colli = this.GetComponent<Collider2D>();
        GravityScaleBackup = rigi.gravityScale;
    }


    // Update is called once per frame
    void Update()
    {
        /*
        if (IsGround && IsLadder && Input.GetAxisRaw("Vertical") != 0)
            this.Colli.isTrigger = true;
        else
            this.Colli.isTrigger = false;
        */

        Moving();
        ChangeAnim();
    }

    

    void ChangeAnim()
    {
        if (AnimController.playerState == AnimControl_base.STATE.HURT)
            return;
        if (rigi.gravityScale == 0)
        {
            AnimController.playerState = AnimControl_base.STATE.CLIMB;
        }else 
        if (IsGround)
        {
            if (Input.GetKey(KeyCode.Z) || JoyStick.Instant.IsShoot)
            {
                AnimController.playerState = AnimControl_base.STATE.SHOOT;
            }
            else if (JoyStick.Instant.GetJoyVectorRaw().x == 0)
            {
                AnimController.playerState = AnimControl_base.STATE.IDLE;
            }
            else
            {
                AnimController.playerState = AnimControl_base.STATE.RUN;
            }
        }
        else
        {
            AnimController.playerState = AnimControl_base.STATE.JUMP;
        }
        
    }

    void Moving()
    {
        IsGround = GroundCheck();

        if (!_IsController)
            return;

        Vector2 SCALE = this.transform.localScale;
        if (movement.x > 0)
        {
            SCALE.x = Mathf.Abs(SCALE.x);
        }else if (movement.x < 0)
        {
            SCALE.x = -Mathf.Abs(SCALE.x);
        }
        this.transform.localScale = SCALE;
        
        if (Input.GetAxisRaw("Vertical") != 0 || JoyStick.Instant.GetJoyVectorRaw().y != 0)
        {
            if (IsLadder)
            {
                rigi.gravityScale = 0;
            }
        }

        if(!IsLadder)
        {
            rigi.gravityScale = GravityScaleBackup;
        }

        if (rigi.gravityScale != 0)
            movement.y = rigi.velocity.y;
        else
        {
            movement.y = JoyStick.Instant.GetJoyVectorRaw().y  * Speed * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.Space) || JoyStick.Instant.IsJump)
        {
            if (IsGround && rigi.gravityScale != 0)
            {
                JoyStick.Instant.SetIsJump(false);
                rigi.AddForce(new Vector2(0, JumpForce));
                IsGround = false;
            }
        }
        movement.x = JoyStick.Instant.GetJoyVectorRaw().x * Speed * Time.deltaTime;
        rigi.velocity = movement;
    }

    bool GroundCheck()
    {
        float lenRay = 0.2f;
        RaycastHit2D[] hits = new RaycastHit2D[10];
        Colli.Cast(Vector2.down, hits, lenRay,true);
        foreach (RaycastHit2D h in hits)
        {
            if (h.collider != null)
            {
                Debug.DrawLine(this.transform.position, h.point, Color.red);
                return true;
            }
        }

        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ladder")
        {
            IsLadder = true;
           
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ladder")
        {
            IsLadder = false;
           
        }
    }

    public void SetIsController(bool IsControl, float TimeControlback)
    {
        this._IsController = IsControl;
        if (IsControl)
            return;
        StartCoroutine(ControlBack(TimeControlback));
    }

    IEnumerator ControlBack(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        _IsController = true;
    }

    public void GetDmg()
    {
        AnimController.playerState = AnimControl_base.STATE.HURT;
        StartCoroutine(HurtExit(1));
    }

    IEnumerator HurtExit(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        AnimController.playerState = AnimControl_base.STATE.IDLE;
    }
}
