﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class AnimPlayer2 : AnimControl_base
{
    [SerializeField] [SpineAnimation] string Run, Idle, Jump, Climb, Shoot;
    SkeletonAnimation Anim;
    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<SkeletonAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        Spine.TrackEntry state = Anim.state.GetCurrent(0);
        
        if (playerState == AnimControl_base.STATE.IDLE )
        {
            if(state == null || state.ToString() != Idle)
                Anim.state.SetAnimation(0, Idle, true);
        }

        if (playerState == AnimControl_base.STATE.RUN)
        {
            if (state == null || state.ToString() != Run)
                Anim.state.SetAnimation(0, Run, true);
        }

        if (playerState == AnimControl_base.STATE.JUMP)
        {
            if (state == null || state.ToString() != Jump)
                Anim.state.SetAnimation(0, Jump, false);
        }

        if (playerState == AnimControl_base.STATE.CLIMB)
        {
            if (state == null || state.ToString() != Climb)
                Anim.state.SetAnimation(0, Climb, true);
            if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0 ||
                JoyStick.Instant.GetJoyVectorRaw().y != 0 || JoyStick.Instant.GetJoyVectorRaw().x != 0)
            {
                Anim.state.TimeScale = 1;
            }
            else
            {
                Anim.state.TimeScale = 0;
            }
        }

        if (playerState == AnimControl_base.STATE.SHOOT)
        {
            Anim.state.SetAnimation(1, Shoot, false);
        }
    }
}
