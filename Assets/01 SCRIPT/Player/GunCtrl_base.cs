﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCtrl_base : MonoBehaviour
{
    [SerializeField] GameObject Bullet_prefab;
    [SerializeField] float DelayFire = 1;
    float Timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        Timer = DelayFire;  
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    protected void Shoot()
    {
        if (Timer > 0)
        {
            Timer -= Time.deltaTime;
            return;
        }

        if (PlayerController.Instant.AnimController.playerState == AnimControl_base.STATE.CLIMB)
        {
            return;
        }

        if (Input.GetKey(KeyCode.Z) || JoyStick.Instant.IsShoot)
        { 
            BulletCtrl B = ObjectPooling.Instant.GetBullet().GetComponent<BulletCtrl>();
            B.Way = PlayerController.Instant.transform.localScale.x;

            B.transform.position = this.transform.position;
            B.gameObject.SetActive(true);

            SoundManager.Instant.PlaySound("boom",GunTriggerTest);
            
            Timer = DelayFire;
        }
    }

    void GunTriggerTest(){
        Debug.Log("Gun Sound end");
    }
}
