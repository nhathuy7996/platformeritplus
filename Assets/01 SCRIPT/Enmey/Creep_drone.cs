﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creep_drone : Creep_base
{
    [SerializeField] PathCreator Path_obj;
    [SerializeField] List<Vector3> paths = new List<Vector3>();
    Vector3 origin = Vector3.zero;
    [SerializeField] int way = 1;
    [SerializeField] float Speed = 10;
    [SerializeField]int currentIndex, targetIndex;

    SpriteRenderer Sprite;
    Animator Anim;


    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<Animator>();
        Sprite = this.GetComponent<SpriteRenderer>();
        paths = Path_obj.List_Points;
        this.origin = this.transform.position;
        currentIndex = 0;
        targetIndex = currentIndex + 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (paths.Count <= 1)
            return;

        this.transform.position = Vector2.MoveTowards(this.transform.position, this.origin + paths[targetIndex], Speed * Time.deltaTime);
      
        if (Vector2.Distance(this.origin + paths[targetIndex], this.transform.position) > 0.1f)
            return;

        if (targetIndex >= paths.Count - 1 || targetIndex == 0)
        {
            way = -way;
   
        }

        currentIndex = targetIndex;
        targetIndex += way;

        if(this.transform.position.x < (this.origin + paths[targetIndex]).x && Sprite.flipX == false)
            Anim.SetBool("TURN", true);

        if (this.transform.position.x > (this.origin + paths[targetIndex]).x && Sprite.flipX == true)
            Anim.SetBool("TURN", true);
    }


    public void EndTurnAround()
    {


        if (this.transform.position.x > (this.origin + paths[targetIndex]).x)
        {
            Sprite.flipX = false;
        }
        else
        {
            Sprite.flipX = true;
        }
    }

    public void StartTurnAround()
    {
        Anim.SetBool("TURN", false);
    }

}
