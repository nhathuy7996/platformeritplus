﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creep_turret : Creep_base
{
    [Header("Creep Turret")]
    [SerializeField] float Force = 100;
    [SerializeField] float TimeDelayEffect = 1;
    float TimerCount = 0;
    [SerializeField] GameObject Bullet;
    [SerializeField] Transform FirePoint;
    // Start is called before the first frame update
    void Start()
    {
        Line = this.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        DetectTarget();
        if (TimerCount > 0)
            TimerCount -= Time.deltaTime;
        Attack();
    }

    protected override void Attack()
    {
        base.Attack();
        if (TimerCountAtk > 0)
        {
            TimerCountAtk -= Time.deltaTime;
            return;
        }

        if (Target == null)
            return;

        TimerCountAtk = AtkSpeed;

        GameObject g = ObjectPooling.Instant.GetTurretBullet();
        g.transform.position = FirePoint.transform.position;
        g.GetComponent<BulletCtrl>().Way = PlayerController.Instant.transform.position.x < this.transform.position.x ? -1 : 1;
        g.SetActive(true);
    }

    protected override void ColliPlayer(GameObject Player)
    {

        if (TimerCount > 0)
            return;
        base.ColliPlayer(Player);
        PlayerController PlayerCtrl = PlayerController.Instant;
        PlayerCtrl.SetIsController(false, TimeDelayEffect);
        PlayerCtrl.GetDmg();
        Vector2 dir = Player.transform.position - this.transform.position;
        float way = PlayerCtrl.transform.position.x < this.transform.position.x ? -1 : 1;
        dir.x = Mathf.Abs(dir.x) * way * Force;
        dir.y = Force;
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Player.GetComponent<Rigidbody2D>().AddForce(dir);

        TimerCount = TimeDelayEffect;

    }

}
