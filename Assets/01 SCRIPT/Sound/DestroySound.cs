﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Event_adv: UnityEvent<string>{

}

public class DestroySound : MonoBehaviour
{
    [SerializeField]
    AudioSource _Audio = null;
    public AudioSource Audio => _Audio;
    public UnityEvent EndSound_event;
    public Event_adv EventSound_adv;

    public delegate void DemoDelegate(string s);
    DemoDelegate H;
    // Start is called before the first frame update
    void Awake()
    {
        _Audio = this.GetComponent<AudioSource>();
        H = ActionTest;
    }

    void Start(){
        StartCoroutine(WaitDone());
         EventSound_adv.AddListener(ActionTest);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator WaitDone(){
        yield return new WaitUntil(()=>!_Audio.isPlaying);
        this.gameObject.SetActive(false);
        EndSound_event.Invoke();
        EventSound_adv.Invoke("hhh");
        H.Invoke("vvvv");
    }

    void ActionTest(string t){
        Debug.Log(t);
    }
}
