﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMoving : Singleton<ButtonMoving>
{
    public Vector2 Direction;

    public void SetDirection(int dir)
    {
        
        if ((DefineDir)dir == DefineDir.UP)
        {
            this.Direction = Vector2.up;
            return;
        }

        if ((DefineDir)dir == DefineDir.DOWN)
        {
            this.Direction = Vector2.down;
            return;
        }

        if((DefineDir)dir == DefineDir.LEFT)
        {
            this.Direction = Vector2.left;
            return;
        }

        if ((DefineDir)dir == DefineDir.RIGHT)
        {
            this.Direction = Vector2.right;
            return;
        }


        this.Direction = Vector2.zero;
    }

    public enum DefineDir
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
