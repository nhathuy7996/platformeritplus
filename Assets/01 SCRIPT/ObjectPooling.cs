﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : Singleton<ObjectPooling>
{
    [SerializeField] GameObject Bullet_prfab;
    [SerializeField] List<GameObject> Bullets = new List<GameObject>();

    [SerializeField] GameObject Bullet_turet_prefab;
    [SerializeField] List<GameObject> Bullet_turets = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetBullet()
    {
        foreach (GameObject b in Bullets)
        {
            if (b.activeSelf)
            {
                continue;
            }
            return b;
        }

        if (Bullet_prfab == null)
        {
            Bullet_prfab = Resources.Load<GameObject>("bullet/bullet");
        }

        GameObject b2 = Instantiate(Bullet_prfab, this.transform.position,Quaternion.identity);
        Bullets.Add(b2);
        return b2;
    }

    public GameObject GetTurretBullet()
    {
        foreach (GameObject b in Bullet_turets)
        {
            if (b.activeSelf)
            {
                continue;
            }
            return b;
        }

        if (Bullet_turet_prefab == null)
        {
            Bullet_turet_prefab = Resources.Load<GameObject>("bullet/bullet_uret");
        }

        GameObject b2 = Instantiate(Bullet_turet_prefab, this.transform.position, Quaternion.identity);
        Bullet_turets.Add(b2);
        return b2;
    }
}
